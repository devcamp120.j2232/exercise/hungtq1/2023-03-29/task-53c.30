package models;

public class Circle extends Shape {
  protected double radius = 1.0;

  public double getArea() {
    return Math.PI*Math.pow(radius, 2);
  }

  public double getPerimeter() {
    return 2*Math.PI*this.radius;
  }

  public double getRadius() {
    return radius;
  }

  public void setRadius(double radius) {
    this.radius = radius;
  }

  public Circle() {
    super();
  }

  public Circle(double radius) {
    this.radius = radius;
  }

  public Circle(String color, Boolean filled, double radius) {
    super(color, filled);
    this.radius = radius;
  }

  @Override
  public String toString() {
    return "Circle[Shape[color="+ this.getColor() + ", filled=" + this.isFilled() + "], radius=" + radius + "]";
  }
}
