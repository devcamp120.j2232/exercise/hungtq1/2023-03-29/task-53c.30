package models;

public class Rectangle extends Shape {
    protected double length = 1.0;
    protected double width = 1.0;

    public Rectangle() {
      super();
    }
    
    public Rectangle(double length, double width) {
      this.length = length;
      this.width = width;
    }

    public Rectangle(String color, Boolean filled, double length, double width) {
      super(color, filled);
      this.length = length;
      this.width = width;
    }

    public Rectangle(String color, Boolean filled) {
      super(color, filled);
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getArea() {
        return this.width * this.length;
    }

    public double getPerimeter() {
      return 2*(this.length + this.width);
    }

    @Override
    public String toString() {
      return "Rectangle[Shape[color="+ this.getColor() + ", filled=" + this.isFilled() + "],length=" + length + ", width=" + width + "]";
    }
}
