import models.*;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle(5);
        System.out.println("Circle: ");
        System.out.println(circle.getArea());
        System.out.println(circle.getPerimeter());

        Rectangle rectangle = new Rectangle(10, 15);
        System.out.println("Rectangle: ");
        System.out.println(rectangle.getArea());
        System.out.println(rectangle.getPerimeter());

        Square square = new Square(10);
        System.out.println("Square: ");
        System.out.println(square.getArea());
        System.out.println(square.getPerimeter());
    }
}
